# Running the project

1. `cd` to the **/bloqhouse-backend** directory
2. `yarn` or `npm install` for downloading node_modules
3. create .env file containing a KEY variable (the value will be provided by email)
3. `yarn start` or `npm run start`


# Enjoy!