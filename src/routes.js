import Request from 'request'
import Boom from 'boom'

let options = {
  url: 'https://api-endpoint.igdb.com/games/?filter[release_dates.platform][eq]=4&limit=20&fields=id,name,summary,cover&order=popularity:desc',
  headers: {
    'user-key': process.env.KEY,
    Accept: 'application/json'
  }
}

const routes = [
  {
    method: 'GET',
    path: '/games',
    handler: async function (request, reply) {
      try {
        await Request.get(options.url,
            {
              headers: options.headers
            },
            (err, httpResponse, body) => {
              if (err) {
                console.log(err)
                return reply(Boom.notFound())
              }
              else {
                return reply(JSON.parse(body))
              }
            })
      } catch (e) {
        return reply(Boom.notFound())
      }

    }
  }
]

export default routes