import Hapi from 'hapi'
import routes from './routes'

const server = new Hapi.Server()
const port = Number(process.env.PORT || 3000)

const connectionProperties = {
  port: port,
  routes: {
    cors: true
  },
  labels: ['app']
}

server.connection(connectionProperties)

const app = server.select('app')

routes.forEach((route) => {
  server.route(route)
})

server.start(err => {
  if (err) {
    throw err
  }
  console.log('Server running at:', server.info.uri)
})
